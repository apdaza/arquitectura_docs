# Procedimientos de desarrollo

## General

## Análisis

![analisis](imagenes/analisis.png "procedimiento de análisis")


## Diseño

![diseño](imagenes/diseno.png "procedimiento de diseño")

## Codificación

![diseño](imagenes/codificacion.png "procedimiento de codificación")

## Pruebas

![pruebas](imagenes/pruebas.png "procedimiento de pruebas")
